const config = {
  title: "DD-Electron Micrographs",
  subtitle:
    "The distributed database of electron micrographs (DDEM) is a public and ownerless distributed repository of electron micrographs. It uses the Open Index Protocol spec to record data in the immutable transactions of the FLO blockchain.",
  ipfs: {
    apiUrl: "https://ipfs.io/ipfs/"
  },
  floExplorer: {
    url: "https://livenet.flocha.in/"
  },
  frontPage: {
    numberOfNewestRecordsToShow: 9,
    aboutTitle: "DDEM is more than just a repository.",
    aboutShortText:
      "Each lab is responsible to host its own content. DDEM is a distributed platform to share data with the community.",
    aboutImage: "aboutLandingPage.png"
  },
  assayPage: {
    numberOfRecordsToShow: 48
  },
  oip: {
    perPage: 12,
    daemonApiUrl: "https://api.oip.io/oip/o5/",
    baseTemplate: "tmpl_B77CF96A",
    requiredTemplates: [],
    addressesWhiteList: ["FLwuRiqxLmyZehZvEK72wyLtmh4NUuU7hv"] //"F6R95XtThjfDr2uGgPPAyG3QS2749osmdC"]
  },
  cardInfo: {
    name: {
      tmpl: "tmpl_20AD45E7",
      name: "name"
    },
    surname: {
      tmpl: "tmpl_B6E9AF9B",
      name: "surname"
    },
    description: {
      tmpl: "tmpl_20AD45E7",
      name: "description"
    },
    avatarRecord: {
      tmpl: "tmpl_20AD45E7",
      name: "avatar"
    }
  },
  imageHandler: {
    thumbnail: {
      tmpl: "tmpl_1AC73C98",
      name: "thumbnailAddress"
    },
    main: {
      tmpl: "tmpl_1AC73C98",
      name: "imageAddress"
    }
  },
  assayHandler: {
    preparedFrom: {
      tmpl: "tmpl_B77CF96A",
      name: "sampleList"
    },
    by: {
      tmpl: "tmpl_B77CF96A",
      name: "executedByList"
    },
    organizations: {
      tmpl: "tmpl_B77CF96A",
      name: "organizationList"
    },
    protocols: {
      tmpl: "tmpl_B77CF96A",
      name: "protocolList"
    },
    legacyIds: {
      tmpl: "tmpl_B77CF96A",
      name: "legacyIdList"
    },
    results: {
      tmpl: "tmpl_B77CF96A",
      name: "resultList"
    }
  },
  sampleHandler: {
    sampleType: {
      tmpl: "tmpl_636E68FA",
      name: "sampleType"
    },
    environmentType: {
      tmpl: "tmpl_636E68FA",
      name: "environmentType"
    },
    location: {
      tmpl: "tmpl_636E68FA",
      name: "location"
    },
    by: {
      tmpl: "tmpl_636E68FA",
      name: "byList"
    },
    organizations: {
      tmpl: "tmpl_636E68FA",
      name: "organizationList"
    },
    preparedFrom: {
      tmpl: "tmpl_636E68FA",
      name: "preparedFromList"
    },
    protocols: {
      tmpl: "tmpl_636E68FA",
      name: "protocolList"
    },
    legacyIds: {
      tmpl: "tmpl_636E68FA",
      name: "legacyIdList"
    }
  }
};

export { config };
