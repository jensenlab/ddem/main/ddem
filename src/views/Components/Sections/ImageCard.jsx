import React from "react";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// core components
import cardStyle from "assets/jss/material-kit-react/views/componentsSections/cardStyle.jsx";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import Popover from "@material-ui/core/Popover";

import { OipApi } from "oip/OipApi";
import { config } from "ddx.config.js";

const api = new OipApi(config.daemonApiUrl);

class ImageCard extends React.Component {
  state = {
    name: "",
    description: "",
    address: require("assets/img/ddx-placeHolder.png"),
    thumbnail: require("assets/img/ddx-placeHolder.png"),
    txid: "",
    open: false
  };

  componentDidMount() {
    if (this.props.data) {
      console.log(this.props.data);
      const recordInfo = this.props.data.record.details;
      if (recordInfo) {
        let name =
          recordInfo[config.cardInfo.name.tmpl][config.cardInfo.name.name];

        const description =
          recordInfo[config.cardInfo.description.tmpl][
            config.cardInfo.description.name
          ];

        const txid = this.props.data.meta.txid;

        const thumbnailAddress =
          recordInfo[config.imageHandler.thumbnail.tmpl][
            config.imageHandler.thumbnail.name
          ];

        const mainImageAddress =
          recordInfo[config.imageHandler.main.tmpl][
            config.imageHandler.main.name
          ];

        this.setState({
          name,
          description,
          txid,
          thumbnail: `${config.ipfs.apiUrl}${thumbnailAddress}`,
          address: `${config.ipfs.apiUrl}${mainImageAddress}`
        });
      }
    }
  }

  handleClick = () => {
    this.setState({
      open: this.state.open ? false : true
    });
  };

  render() {
    const { classes } = this.props;
    const styles = {
      media: {
        height: 0,
        paddingTop: "56.25%", // 16:9,
        marginTop: "30"
      }
    };
    return (
      <Card className={classes.card} style={{ height: "200px" }}>
        <CardActionArea onClick={this.handleClick}>
          <CardMedia
            component="img"
            alt="Record Image"
            className={classes.media}
            height="150"
            image={this.state.thumbnail}
            title={this.state.name}
          />
          <Popover open={this.state.open}>
            <CardMedia
              component="img"
              alt="Record Image"
              className={classes.media}
              image={this.state.address}
              title={this.state.name}
            />
          </Popover>
          <CardContent>
            <Typography
              noWrap={true}
              variant="body2"
              style={{ overflowWrap: "break-word" }}
            >
              {this.state.description}
            </Typography>
            <Typography
              style={{
                fontSize: "8px",
                overflowWrap: "break-word",
                fontStyle: "italic"
              }}
            >
              {this.state.txid}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    );
  }
}

export default withStyles(cardStyle)(ImageCard);
