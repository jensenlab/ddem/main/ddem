import React from "react";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// core components
import pillsStyle from "assets/jss/material-kit-react/views/componentsSections/pillsStyle.jsx";
import Grid from "@material-ui/core/Grid";
import ImageCard from "views/Components/Sections/ImageCard.jsx";
// import Pagination from "components/Pagination/Pagination.jsx";
// import SectionSort from "./SectionSort";

import { OipApi } from "oip/OipApi";
import { config } from "ddx.config.js";
import Pagination from "@material-ui/lab/Pagination";
import { Typography } from "@material-ui/core";

const api = new OipApi(config.daemonApiUrl);

class SectionPills extends React.Component {
  constructor() {
    super();
    this.state = {
      records: [],
      searching: false,
      oipRefs: [],
      page: 1,
      totalCount: 0,
      txid: ""
    };
  }

  componentDidUpdate() {
    if (this.props.results !== this.state.oipRefs) {
      this.setState(
        {
          oipRefs: this.props.results,
          totalCount: this.props.results.length,
          page: 1
        },
        this.updateGrid
      );
    }
  }

  changePage = (e, v) => {
    if (this.state.page !== v) {
      console.log(`${this.state.page} -> ${v}`);
      this.setState(
        {
          page: v
        },
        this.updateGrid
      );
    }
  };

  sliceRecords() {
    const n = config.assayPage.numberOfRecordsToShow;
    return this.state.oipRefs.slice(
      (this.state.page - 1) * n,
      this.state.page * n
    );
  }

  updateGrid = () => {
    this.setState(
      {
        searching: true,
        records: []
      },
      () =>
        api
          .getRecordsByIdList(
            this.sliceRecords(),
            config.assayPage.numberOfRecordsToShow
          )
          .then(json => {
            this.setState({
              records: json.results,
              txid: json.results.map(d => d.meta.txid)[0],
              searching: false
            });
          })
    );
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.section}>
        <div className={classes.container}>
          <Grid
            container
            alignItems="center"
            justify="space-between"
            direction="row"
            spacing={2}
            style={{ padding: "20px" }}
          >
            <Typography>
              {this.state.searching
                ? "Searching the blockchain..."
                : this.state.records.length === 0
                ? "No records were found."
                : `Records found: ${this.state.totalCount}`}
            </Typography>
            <Pagination
              count={Math.ceil(
                this.state.totalCount / config.assayPage.numberOfRecordsToShow
              )}
              page={this.state.page}
              style={{ padding: "20px" }}
              color="primary"
              className={classes.pagination}
              onChange={this.changePage}
            />
          </Grid>
          <div id="navigation-pills">
            <div className={classes.root}>
              <Grid
                container
                alignItems="flex-start"
                justify="flex-start"
                direction="row"
                spacing={2}
              >
                {this.state.records.map((value, index) => {
                  return (
                    <Grid item xs={12} sm={4} md={3} lg={2} key={index}>
                      <ImageCard data={value} />
                    </Grid>
                  );
                })}
              </Grid>

              <Grid container spacing={24}></Grid>
            </div>
          </div>
          <Grid
            container
            alignItems="flex-end"
            justify="center"
            direction="row"
            spacing={2}
            style={{ padding: "20px" }}
          >
            <Pagination
              count={Math.ceil(
                this.state.totalCount / config.assayPage.numberOfRecordsToShow
              )}
              page={this.state.page}
              color="primary"
              className={classes.pagination}
              onChange={this.changePage}
            />
          </Grid>
        </div>
      </div>
    );
  }
}

export default withStyles(pillsStyle)(SectionPills);
